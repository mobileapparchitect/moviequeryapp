package com.arkangelx.movieapp

import com.arkangelx.movieapp.api.ComicSearchApi
import com.arkangelx.zavatest.base.api.RestClientImplementation
import com.arkangelx.zavatestproject.model.ComicQueryResponse
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Test

class RestClientImplementationTest {
    private val comicSearchApi: ComicSearchApi = mock()

    @Test
    fun shouldGetComicsComplte() {
        val restClientImplementation = RestClientImplementation(comicSearchApi)
        stubRemoteGetComics(Single.just(ComicQueryResponse()))
        val testObservable = restClientImplementation.retrieveComicList("", "", "").test()
        testObservable.assertComplete()
    }


    @Test
    fun getComicsReturnsData() {
        val restClientImplementation = RestClientImplementation(comicSearchApi)
        val response = ComicQueryResponse()
        stubRemoteGetComics(Single.just(response))
        val testObservable = restClientImplementation.retrieveComicList("", "", "").test()
        testObservable.assertValue(response)
    }

    private fun stubRemoteGetComics(single: Single<ComicQueryResponse>) {
        whenever(comicSearchApi.retrieveComics(any(), any(), any()))
            .thenReturn(single)
    }
}