package com.arkangelx.movieapp.home.presenter

import com.arkangelx.movieapp.api.ComicSearchApi
import com.arkangelx.zavatestproject.model.ComicQueryResponse
import com.nhaarman.mockito_kotlin.*
import com.util.TestSchedulerProvider
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.TestScheduler
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.*
import kotlin.test.assertNotNull

class ComicBookPresenterTest {


    private val view: HomeView = mock()
    private val comicSearchApi: ComicSearchApi = mock()
    private lateinit var presenter: ComicBookPresenter
    private lateinit var testScheduler: TestScheduler


    @Before
    fun setup() {
        val compositeDisposable = CompositeDisposable()
        testScheduler = TestScheduler()
        val testSchedulerProvider = TestSchedulerProvider(testScheduler)
        presenter = ComicBookPresenter(comicSearchApi, compositeDisposable, testSchedulerProvider)
        presenter.attachView(view)
    }
    @Test
    fun viewNotNull() {
        assertNotNull(view)
    }

    @After
    fun tearDown() {
        presenter.detachView()
    }

    @Test
    fun getCurrentTimeStamp() {
        var value = presenter.getTimeStamp()
        assertNotNull(value)
    }


    @Test
    fun getHashValue() {
        var value = presenter.md5(UUID.randomUUID().toString())
        assertNotNull(value)
    }

    @Test
    fun test_getRepos_should_callSuccess() {
        val mockedResponse: ComicQueryResponse = mock()

        doReturn(Single.just(mockedResponse))
            .`when`(comicSearchApi)
            .retrieveComics(any(), any(), any())

        presenter.getComics()

        testScheduler.triggerActions()

        verify(view).showProgress()
        verify(view).onSearchResponse(mockedResponse.data?.results)
        verify(view).hideProgress()

    }

 
}