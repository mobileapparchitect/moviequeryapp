package com.base.mvp

import androidx.annotation.StringRes


interface BaseView {
    fun setPresenter(presenter: BasePresenter<*>){}

    val isNetworkConnected: Boolean

    fun onError(@StringRes resId: Int) {}

    fun onError(message: String) {}

    fun showMessage(message: String) {}

    fun showMessage(@StringRes resId: Int) {}

    fun hideKeyboard() {}
}