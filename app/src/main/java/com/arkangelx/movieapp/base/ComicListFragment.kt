package com.arkangelx.movieapp.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.arkangelx.movieapp.MainActivity
import com.arkangelx.movieapp.R
import com.arkangelx.movieapp.home.ComicListAdapter
import com.arkangelx.movieapp.home.di.DaggerHomeActivityComponent
import com.arkangelx.movieapp.home.di.HomeActivityModule
import com.arkangelx.movieapp.home.presenter.ComicBookPresenter
import com.arkangelx.movieapp.home.presenter.HomeView
import com.arkangelx.zavatestproject.model.Result
import com.base.ComicQueryApp
import kotlinx.android.synthetic.main.comic_list.*
import javax.inject.Inject

class ComicListFragment() : BaseFragment(), HomeView {
    lateinit var comicListAdapter: ComicListAdapter
    @Inject
    lateinit var homePresenter: ComicBookPresenter

    override val isNetworkConnected: Boolean
        get() = ComicQueryApp.hasNetwork(activity!!)

    override fun setUp(view: View) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val component = (activity as MainActivity).getAppcomponent()

        DaggerHomeActivityComponent.builder().appComponent(component)
            .homeActivityModule(HomeActivityModule())
            .build()
            .inject(this)
        setPresenter(homePresenter)
        homePresenter.attachView(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.comic_list, container, false)
        return view
    }

    override fun onSearchResponse(list: List<Result>?) {
        list?.let {
            comicListAdapter = ComicListAdapter(it.toMutableList())
            comic_list?.apply {
                adapter = comicListAdapter
                layoutManager = LinearLayoutManager(context)
            }
        }
    }


    override fun onResume() {
        super.onResume()
        if (isNetworkConnected) {
            homePresenter.getComics()
        }
    }


}