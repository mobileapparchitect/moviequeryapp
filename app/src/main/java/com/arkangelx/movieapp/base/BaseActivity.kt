package com.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.base.di.component.AppComponent
import com.base.mvp.BasePresenter
import com.base.mvp.BaseView


abstract class BaseActivity : AppCompatActivity(), BaseView {

    private var presenter: BasePresenter<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onActivityInject()
    }

    protected abstract fun onActivityInject()

    fun getAppcomponent(): AppComponent = ComicQueryApp.appComponent

    override fun setPresenter(presenter: BasePresenter<*>) {
        this.presenter = presenter
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter?.detachView()
        presenter = null
    }

    override val isNetworkConnected: Boolean
        get() = ComicQueryApp.hasNetwork(this)

}
