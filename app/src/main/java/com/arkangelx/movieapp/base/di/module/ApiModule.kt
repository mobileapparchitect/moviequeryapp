package com.arkangelx.zavatestproject.di.module

import com.arkangelx.zavatest.base.api.RestClientImplementation
import com.arkangelx.movieapp.api.ComicSearchApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
class ApiModule {

    @Provides
    @Singleton
    fun providesComicApi(retrofit: Retrofit): ComicSearchApi =
        retrofit.create(ComicSearchApi::class.java)


    @Provides
    @Singleton
    fun provideRestClientImplementation(comicSearchApi: ComicSearchApi):
            RestClientImplementation = RestClientImplementation(
        comicSearchApi
    )


}