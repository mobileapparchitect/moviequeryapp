package com.arkangelx.zavatest.base.api

import com.arkangelx.zavatestproject.model.ComicQueryResponse
import com.arkangelx.movieapp.api.ComicSearchApi
import io.reactivex.Single

class RestClientImplementation(var comicSearchApi: ComicSearchApi) :
    RestClient {
    override fun retrieveComicList(
        timeStamp: String,
        apiKey: String,
        hashVaue: String
    ): Single<ComicQueryResponse> {
        return comicSearchApi!!.retrieveComics(timeStamp, apiKey, hashVaue)
    }


}