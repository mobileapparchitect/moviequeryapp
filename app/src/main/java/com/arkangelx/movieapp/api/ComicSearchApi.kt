package com.arkangelx.movieapp.api

import com.arkangelx.zavatestproject.model.ComicQueryResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface ComicSearchApi {
    @GET("public/comics")
    fun retrieveComics(@Query("ts") timestamp: String, @Query("apikey") apiKey: String, @Query("hash") hashValue: String)
            : Single<ComicQueryResponse>
}