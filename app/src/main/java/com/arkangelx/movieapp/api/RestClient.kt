package com.arkangelx.zavatest.base.api

import com.arkangelx.zavatestproject.model.ComicQueryResponse
import io.reactivex.Single
import retrofit2.http.Query

interface RestClient {


    fun retrieveComicList(
        @Query("ts") timeStamp: String,
        @Query("apiKey") apiKey: String,
        @Query("hash") hashVaue: String
    ): Single<ComicQueryResponse>
}