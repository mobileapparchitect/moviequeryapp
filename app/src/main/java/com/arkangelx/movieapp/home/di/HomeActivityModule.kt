package com.arkangelx.movieapp.home.di

import com.arkangelx.movieapp.api.ComicSearchApi
import com.base.di.ActivityScope
import com.base.util.AppSchedulerProvider

import com.arkangelx.movieapp.home.presenter.ComicBookPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable

@Module
class HomeActivityModule {

    @Provides
    @ActivityScope
    internal fun providesHomePresenter(api: ComicSearchApi, disposable: CompositeDisposable, scheduler: AppSchedulerProvider): ComicBookPresenter =
            ComicBookPresenter(api, disposable, scheduler)
}
