package com.arkangelx.movieapp.home.presenter

interface PresenterContract {
    fun getComics()
    fun searchForComicByTitle(comicTitle: String)

}