package com.arkangelx.movieapp.home.presenter

import com.arkangelx.zavatestproject.model.Result
import com.base.mvp.BaseView

interface HomeView : BaseView {
    fun onSearchResponse(list: List<Result>?)
    fun showProgress(){}
    fun hideProgress(){}
    fun noResult(){}
}
