package com.arkangelx.movieapp.home


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.arkangelx.zavatestproject.model.Result
import com.bumptech.glide.Glide


class ComicListAdapter(
    private var listOfComicResults: MutableList<Result?>
) : RecyclerView.Adapter<ComicListAdapter.ComicItemViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ComicItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(com.arkangelx.movieapp.R.layout.comic_list_item, parent, false)
        return ComicItemViewHolder(view)
    }

    override fun getItemCount() = listOfComicResults.size

    override fun onBindViewHolder(holder: ComicItemViewHolder, position: Int) {
        val imageExtension = ".jpg"
        var currentItem = listOfComicResults.get(position)
        val titleTextView = holder.item
        val imageView = holder.imageView
        titleTextView.setText(currentItem?.title)
        Glide.with(imageView.context).load(currentItem?.thumbnail?.path?.plus(imageExtension))
            .thumbnail(0.1f).into(imageView);
    }


    fun clean() {
        listOfComicResults.clear()
        notifyDataSetChanged()
    }

    class ComicItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var item: TextView
        var imageView: ImageView

        init {
            item = itemView.findViewById(com.arkangelx.movieapp.R.id.title) as TextView
            imageView = itemView.findViewById(com.arkangelx.movieapp.R.id.imageView) as ImageView
        }


    }
}