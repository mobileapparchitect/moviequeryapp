package com.arkangelx.movieapp.home.presenter

import com.arkangelx.movieapp.BuildConfig
import com.arkangelx.movieapp.api.ComicSearchApi
import com.base.mvp.BasePresenter
import com.base.util.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable
import java.security.NoSuchAlgorithmException
import javax.inject.Inject

class ComicBookPresenter @Inject constructor(
    var comicSearchApi: ComicSearchApi,
    disposable: CompositeDisposable,
    scheduler: SchedulerProvider
) : BasePresenter<HomeView>(disposable, scheduler) ,PresenterContract{


    private var currentTimeStamp = ""
    private var hackedTimeStamp = "thesoer"
    private var hackedApiKey = "001ac6c73378bbfff488a36141458af2"
    private var hackedHashValue = "72e5ed53d1398abb831c3ceec263f18b"

    private var MARVEL_PUBLIC_KEY = "0c304983b8f3a4e13f7c052e8b664eab"
    private var MARL_PRIVATE_KEY = "ce139dbd514ca4cf6e845b7b0fcdc1bc949a01ed"

    override fun getComics() {
        view?.showProgress()

        currentTimeStamp = getTimeStamp()
        // md5(ts+privateKey+publicKey)API SAYS NO!!!
        //used credentials from here
        //https://stackoverflow.com/questions/52670530/marvel-api-and-insomnia-or-postman-how-do-i-pass-the-hash-value-thats-requir

        var hashValue = generateMd5Hash(currentTimeStamp, MARL_PRIVATE_KEY, MARVEL_PUBLIC_KEY)
        if (BuildConfig.FAKE_CREDENTIAILS) {
            currentTimeStamp = hackedTimeStamp
            hashValue = hackedHashValue
            MARVEL_PUBLIC_KEY = hackedApiKey
        }

        hashValue?.let {

            disposable.add(
                comicSearchApi.retrieveComics(currentTimeStamp, MARVEL_PUBLIC_KEY, it)
                    .subscribeOn(scheduler.io())
                    .observeOn(scheduler.ui())
                    .subscribe(
                        { result ->
                            view?.hideProgress()
                            view?.onSearchResponse(result?.data?.results)
                            if (result.data?.results == null || result.data?.results!!.isEmpty()) {
                                view?.noResult()
                            }
                        },
                        { _ ->
                            view?.hideProgress()
                            view?.onError("Error")
                        })
            )
        }
    }

    override fun searchForComicByTitle(comicTitle: String) {
        //TODO
    }

    fun getTimeStamp(): String {
        var timeStamp = System.currentTimeMillis() / 1000
        var currentTimeStamp = timeStamp.toString()
        return currentTimeStamp
    }

    fun md5(s: String): String {
        val MD5 = "MD5"
        try {
            // Create MD5 Hash
            val digest = java.security.MessageDigest
                .getInstance(MD5)
            digest.update(s.toByteArray())
            val messageDigest = digest.digest()
            // Create Hex String
            val hexString = StringBuilder()
            for (aMessageDigest in messageDigest) {
                var h = Integer.toHexString(0xFF and aMessageDigest.toInt())
                while (h.length < 2)
                    h = "0$h"
                hexString.append(h)
            }
            return hexString.toString()

        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }

        return ""
    }

    private fun generateMd5Hash(timestamp: String, privateKey: String, publicKey: String): String? {
        return md5("{$timestamp$privateKey$publicKey}")
    }

}