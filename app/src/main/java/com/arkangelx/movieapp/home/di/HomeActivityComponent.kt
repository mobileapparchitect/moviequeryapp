package com.arkangelx.movieapp.home.di

import com.arkangelx.movieapp.MainActivity
import com.arkangelx.movieapp.base.ComicListFragment
import com.arkangelx.movieapp.home.di.HomeActivityModule
import com.base.di.ActivityScope
import com.base.di.component.AppComponent


import dagger.Component

@ActivityScope
@Component(
    dependencies = arrayOf(AppComponent::class),
    modules = arrayOf(HomeActivityModule::class)
)
interface HomeActivityComponent {

    fun inject(mainActivity: MainActivity)

    fun inject(comicListFragment: ComicListFragment)
}
