package com.arkangelx.movieapp


import android.os.Bundle
import com.arkangelx.movieapp.base.ComicListFragment
import com.arkangelx.movieapp.home.di.DaggerHomeActivityComponent
import com.arkangelx.movieapp.home.di.HomeActivityModule
import com.arkangelx.movieapp.home.presenter.ComicBookPresenter
import com.base.BaseActivity
import javax.inject.Inject


class MainActivity : BaseActivity() {


    @Inject
    lateinit var homePresenter: ComicBookPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }


    override fun onResume() {
        super.onResume()
        homePresenter.getComics()

        val fragmentManager = supportFragmentManager
        val ft = fragmentManager.beginTransaction()

        ft.add(
            R.id.fragment_container,
            ComicListFragment(),
            ComicListFragment::class.java.simpleName
        )
        ft.commit()

    }


    override fun onActivityInject() {
        DaggerHomeActivityComponent.builder().appComponent(getAppcomponent())
            .homeActivityModule(HomeActivityModule())
            .build()
            .inject(this)
    }


    override val isNetworkConnected: Boolean
        get() = true

}
